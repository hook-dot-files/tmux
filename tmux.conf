# Sets prefix from <Control>+b to <Control>+a (as in GNU Screen)
# I'll find a more comfortable prefix when I create my own keyboard layout
set-option -g prefix C-a 
unbind-key C-b
bind-key C-a send-prefix

# Sets status bar updating to 2 seconds (needed for tmux-mem-cpu-load to update faster)
set -g status-interval 2

# Set a bigger history
set -g history-limit 1000

# Status bar with prettier colours and more information.
# On left: hostname
# On right: RAM, CPU usage and load avg. (needs tmux-mem-cpu-load installed)
set -g status-bg black
set -g status-fg cyan
set -g status-left-length 20
set -g status-left "#[fg=magenta]#h | #S"
set -g status-right-length 44
set -g status-right "#[fg=blue]#(tmux-mem-cpu-load --color --interval 2)"

# Highlight active window
set-window-option -g window-status-current-bg green
set-window-option -g window-status-current-fg black

# Force Vi(m)-like default keybidings
setw -g mode-keys vi
setw -g status-keys vi

#TODO: have pane movement, resizing, focus and splitting the same as in Vim

# More logical bindings for screen splitting
unbind %
unbind '"'
unbind |
unbind -
#TODO: Migrate this block also to Vim
bind | split-window -h
bind - split-window -v

# More logical bindings for pane resizing
bind + resize-pane -U 4
bind - resize-pane -D 4
bind < resize-pane -L 4
bind > resize-pane -R 4

# GNU Screen-like switching between last two windows
bind C-a last-window

#TODO: A more logical - Vim-like, if you will - command to search through windows
unbind /
bind / command-prompt "find-window %%"

# A more logical - KDE-like - way of moving left and right through windows
unbind ,
bind . next-window
bind , previous-window

# A more logical - GNU Screen like - binding for renaming
unbind A
bind A command-prompt "rename-window %%"

# Keeps windows open after the command exits
set -g remain-on-exit on
#TODO - Figure out how to keep windows and panes in sessions *actually* alive after the command finishes.
