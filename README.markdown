My settings for [tmux][].

Most changes make the keybindings closer to GNU Screen (e.g. `Ctrl+A` instead of the default `Ctrl+B`) and Vim.

The other small changes are just small visual niceties.


# Installation

Depending on whether you have write access to this repository or not, choose one of the following:


## Read-only (default)

~~~~.sh
git clone https://gitlab.com/hook-dot-files/tmux.git .tmux
ln -s ~/.tmux/tmux.conf ~/.tmux.conf
~~~~


## Read-write access

~~~~.sh
git clone git@gitlab.com:hook-dot-files/tmux.git .tmux
ln -s ~/.tmux/tmux.conf ~/.tmux.conf
~~~~


## Optional dependencies

This [tmux][] config file is written in a way to show RAM and CPU usage as well as the system’s load.

For this to work, you have to install [tmux-mem-cpu-load][tmux-mem]

[tmux]: https://tmux.github.io/
[tmux-mem]: https://github.com/thewtex/tmux-mem-cpu-load
